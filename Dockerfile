#Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copia de archivos, añade todo desde donde estoy
ADD . /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3002

#Comando que requiere la app para ejecutar
CMD ["npm", "start"]
