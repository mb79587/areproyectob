  //version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

var requestjson=require('request-json');
var path = require('path');
app.listen(port);
var urlMovimientos ="https://api.mlab.com/api/1/databases/bdbanca3mb79587/collections/movimientos/?apiKey=krrpcDDp42z0DaTCN__IuLf8LgnAcYRO";
var movimientosMLab = requestjson.createClient(urlMovimientos);
var urlUsuarios ="https://api.mlab.com/api/1/databases/bdbanca3mb79587/collections/usuarios?apiKey=krrpcDDp42z0DaTCN__IuLf8LgnAcYRO";
var usuarioMLab = requestjson.createClient(urlUsuarios);
var urlNorthwind = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
var northwindMlab = requestjson.createClient( urlNorthwind );


console.log('Are, todo list RESTful API server started on: ' + port);


app.post( '/loginUser', function( req, res ){
      var password = req.body.password;
      var email = req.body.email;
      var query = urlUsuarios + '&q={"email":"' + email + '"}&l=1';
      console.log(query);
      var mlabApi = requestjson.createClient(query);

      mlabApi.get('', (err, resM, body) => {
        var objUserResult = {};
        if(err){
          console.error(err);
        }else {
          var objUser = {};
          objUser = body[0];
          if(objUser != undefined){
            if(objUser.password == password){
              objUserResult = {
                id_usuario : objUser.id_usuario,
                nombre : objUser.nombre,
                apellido : objUser.apellido,
                email : objUser.email
              }
            }
          }
        }
        res.send( objUserResult );
      });
});

app.get('/usuarios',function(req,res){
 usuarioMLab.get('', function(err,resM,body){
    if(err){
       console.log(err  );
   }else{
       res.send(body);
   }
 });
});

app.get('/movimientos',function(req,res){
 movimientosMLab.get('', function(err,resM,body){
    if(err){
       console.log(err  );
   }else{
       res.send(body);
   }
 });
});

app.get('/api',function(req,res){
  northwindMlab.get('', function(err,resM,body){
    if(err){
       console.log(err  );
   }else{
       res.send(body);
   }
 });
});

app.post( '/registraMov', function( req, res, next ){
    console.log("Registrando movimiento");

    var promiseRegistro = new Promise(function (resolve, reject){
    movimientosMLab.post('', req.body, function(err, res, body) {
    if(res.statusCode == 200){
    resolve(res.statusCode);
    }else {
    reject(err);
    }
    // return console.log(res.statusCode);
    });
    });
    res.send("El movimiento se registro satisfactoriamente");
    }
    );

    app.post( '/registraUsuario', function( req, res, next ){
        console.log(" Registrando usuario");

        var promiseRegistro = new Promise(function (resolve, reject){
        usuarioMLab.post('', req.body, function(err, res, body) {
        if(res.statusCode == 200){
        resolve(res.statusCode);
        }else {
        reject(err);
        }
        // return console.log(res.statusCode);
        });
        });
        res.send("El usuario se registro satisfactoriamente");
        }
        );
